# The Band Database
This if from xyBook's C 196: Mobile Application Development 6.2: The Band Database app.

| Model             | View                 | Controller           |
|-------------------|----------------------|----------------------|
| Band.java         | activity_details.xml | DetailsActivity.java |
| BandDatabase.java | activity_list.xml    | DetailsFragment.java |
|                   | fragment_details.xml | ListActivity.java    |
|                   | fragment_list.xml    | ListFragment.java    |
